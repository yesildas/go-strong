package main

import (
	"errors"
	"fmt"
)

type Address struct {
	street string
	city   string
}

type Student struct {
	matriculationNumber int
	name                string
	address             Address
	grades              map[string]float32
}

func (a *Address) String() string {
	return fmt.Sprintf("%s, %s", a.street, a.city)
}

func (s *Student) String() string {
	return fmt.Sprintf("%d %s, %s", s.matriculationNumber, s.name, s.address.String())
}

func (s *Student) AddGrade(lecture string, grade float32) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("Panic occured: %s\n", err)
		}
	}()
	if s.grades == nil {
		s.grades = make(map[string]float32)
	}
	if grade < 1.0 || grade > 5.0 {
		panic(fmt.Sprintf("The grade %f is not within the range 1.0 - 5.0.", grade))
	}
	s.grades[lecture] = grade
}

func (s *Student) GetOverallAverageGrade() float32 {
	var sum float32
	for _, grade := range s.grades {
		sum += grade
	}

	return sum / float32(len(s.grades))
}

type SharedFlat struct {
	address  Address
	students []Student
}

func (s *SharedFlat) AddStudent(student Student) error {
	for _, v := range s.students {
		if student.matriculationNumber == v.matriculationNumber {
			return errors.New("student is already moved in")
		}
	}
	s.students = append(s.students, student)

	return nil
}

func (s *SharedFlat) GetNames() []string {
	var names []string
	for _, student := range s.students {
		names = append(names, student.String())
	}

	return names
}

func main() {
	address := Address{
		street: "Alteburgstraße 150",
		city:   "72762 Reutlingen",
	}

	student := Student{
		matriculationNumber: 7211,
		name:                "Max Müller",
		address:             address,
	}

	student.AddGrade("Enterprise Service Development", 1.0)
	student.AddGrade("Software Architecture", 1.2)
	student.AddGrade("Cloud Computing", 0.2)

	fmt.Println(student.String())
	fmt.Println("Overall Average Grade:", student.GetOverallAverageGrade())

	sharedFlat := SharedFlat{
		address: address,
	}

	err := sharedFlat.AddStudent(student)
	if err != nil {
		fmt.Printf("Student %d has already moved in.\n", student.matriculationNumber)
	}

	err = sharedFlat.AddStudent(student)
	if err != nil {
		fmt.Printf("Student %d has already moved in.\n", student.matriculationNumber)
	}

	fmt.Println(sharedFlat.GetNames())
}
